/*
 Create frame object with FlowLayoutDemo()
 Pack the frame and make it visible
 */

import javax.swing.*;
import java.awt.*;


public class ShowFlowLayoutDemo 
{

	public static void main(String[] args) 
	{
		FlowLayoutDemo frame = new FlowLayoutDemo();
		
        frame.pack();
		frame.setVisible(true);

	}

}
