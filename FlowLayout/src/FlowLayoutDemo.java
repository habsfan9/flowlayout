/*
 Construct new FlowLayout object called demoLayout
 
 Method FlowLayoutDemo()
 	Create panels and set the layout to FlowLayout
 	
 	Add the panels
 	
 	Add buttons 1-3 to panel 1
 	Add buttons 4-6 to panel 2
 	
 	Set the orientation of the components from left to right
 	
 	Center the frame
 	
 	Set the title, layout, location of the frame
 	Set the program to exit when window is closed
 
 */

import javax.swing.*;
import java.awt.*;

public class FlowLayoutDemo extends JFrame
{
	FlowLayout demoLayout = new FlowLayout();
	
	public FlowLayoutDemo()
	{
		JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();

        panel1.setLayout(new FlowLayout());
        panel2.setLayout(new FlowLayout());
        
        Container contentPane = getContentPane();
        
        contentPane.add(panel1);
        contentPane.add(panel2);
        
        panel1.add(new JButton("Button 1"));
        panel1.add(new JButton("Button 2"));
        panel1.add(new JButton("Button 3"));
        
        panel2.add(new JButton("Button 4"));
        panel2.add(new JButton("Button 5"));
        panel2.add(new JButton("Button 6"));
        
        setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        
        demoLayout.setAlignment(FlowLayout.CENTER);
        
        setLayout(demoLayout);
        setTitle("FlowLayout Demo");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
